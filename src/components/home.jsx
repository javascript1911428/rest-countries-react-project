import React from "react";
import { useState, useEffect } from "react";
import './home.css'
import { DarkModeProvider, useDarkMode } from "./darkmode";
import icon1 from "../assets/icon.svg"
import icon2 from "../assets/iconlight.svg"

function Homepage() {
    const {darkMode, toggleDarkMode} = useDarkMode();
    return (
    <div className={darkMode ? "light-mode" : "dark-mode"}>
        <header>
            <h1 className="header-heading">Where in the world?</h1>
            {/* <FontAwesomeIcon icon="fa-sharp fa-solid fa-moon" /> */}
            <div className="mode" onClick={toggleDarkMode}>
            <img
            src={darkMode ? icon2 : icon1}
            alt=""
            className="icon"
          />
          <p className="mode">DarkMode</p>
            </div>
        </header>
    </div>
    )
}

export default Homepage