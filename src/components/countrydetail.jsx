import React, { useState, useEffect } from "react";
import { useParams, useNavigate } from "react-router-dom";
import Homepage from "./home";
import "./countrydetail.css";
import { useDarkMode } from "./darkmode";

function CountryDetail() {
  const { cca2 } = useParams();
  const navigate = useNavigate();
  const [country, setCountry] = useState(null);
  const { darkMode } = useDarkMode();

  useEffect(() => {
    fetch(`https://restcountries.com/v3.1/alpha/${cca2}`)
      .then((response) => {
        if (!response.ok) {
          throw new Error("Network response was not ok");
        }
        return response.json();
      })
      .then((data) => {
        const countryData = data[0];
        setCountry(countryData);
      })
      .catch((error) => {
        console.error(error);
      });
  }, [cca2]);

  function handleBorderCountries(border) {
    navigate(`/country/${border}`);
  }

  if (!country) {
    return <div>Loading...</div>;
  }

  return (
    <>
      <Homepage />
      <div className="total">
        <div className="back-button-container" id={`${darkMode ? "light-mode" : "dark-mode"}`}>
            <button onClick={() => navigate(-1)}>Back</button>
        </div>
        <div className= "country-detail" id={`${darkMode ? "light-mode" : "dark-mode"}`}>
          <div className="details-container">
            <div className="img-container">
              <img
                src={country.flags["png"]}
                alt={country.name.nativeName.common}
              />
            </div>
            <div className="country-info">
              <h2>{country.name.common}</h2>
              <div className="detail-country-info">
                <div className="country-inner-info1">
                  <p>
                    <b>Native Name:</b>
                    {
                      Object.values(country.name.nativeName)[
                        Object.values(country.name.nativeName).length - 1
                      ].common
                    }
                  </p>
                  <p>
                    Population:{country.population}
                  </p>
                  <p>
                    Region:{country.region}
                  </p>
                  <p>
                    Subregion:{country.subregion}
                  </p>
                  <p>
                    Capital:{country.capital}
                  </p>
                </div>

                <div className="country-inner-info2">
                  <p>
                    Top LevelDomain:{country.tld[0].slice(1)}
                  </p>
                  <p>
                    Currencies:{Object.values(country.currencies)[0].name}
                  </p>
                  <p>
                    Languages:{" "}
                    {Object.values(country.languages).join(", ")}
                  </p>
                </div>
              </div>

              {country.borders && (
                <div className="border-names">
                  <b>Border Countries:</b>
                  {country.borders.map((border, index) => (
                    <button
                      key={index}
                      onClick={() => {handleBorderCountries(border)}}
                      className="border-button"
                    >
                      {border}
                    </button>
                  ))}
                </div>
              )}
            </div>
          </div>
        </div>
      </div>
    </>
  );
}

export default CountryDetail;
