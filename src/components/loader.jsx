
import React, { useState, useEffect } from 'react';
import './loader.css';

function Loader() {
  const [isLoading, setIsLoading] = useState(true);

  useEffect(() => {
    setTimeout(() => {
      setIsLoading(false);
    }, 3000);

    return () => {
      
    };
  }, []);

  return (
    isLoading ? (
      <div className="loader">
        <div className="loader-spinner"></div>
      </div>
    ) : null
  );
}

export default Loader;


