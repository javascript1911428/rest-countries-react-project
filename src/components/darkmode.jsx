import React, { createContext, useContext, useState } from 'react';

//here, a DarkModeContext is created using createContext(). 
//This context will be used to share the dark mode state and the 
//"toggleDarkMode" function throughout the component tree 
const DarkModeContext = createContext();

// A custom hook named useDarkMode is defined. This hook allows components
//within your application to access "darkMode" state and the "toggleDarkMode"
//function from the context using "useContext" hook.

export const useDarkMode = () => {
  return useContext(DarkModeContext);
};



export const DarkModeProvider = ({ children }) => {
  const [darkMode, setDarkMode] = useState(false);

  const toggleDarkMode = () => {
    setDarkMode(!darkMode);
  };

  return (
    <DarkModeContext.Provider value={{ darkMode, toggleDarkMode }}>
      {children}
    </DarkModeContext.Provider>
  );
};
