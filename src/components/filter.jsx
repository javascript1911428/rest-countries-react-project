import React from "react";
import "./filter.css";
import { useDarkMode } from "./darkmode";

function Filter({
  handleInputchange,
  searchCountry,
  handleRegionChange,
  regions,
  subregions,
  handleSubregionChange,
  handlePopulationSortChange,
  handleAreaSortChange,
}) {
  const { darkMode } = useDarkMode();
  return (
    <div className={`filter-container ${darkMode ? "light-mode" : "dark-mode"}`}>
      <input
        type="search"
        placeholder="Select country..."
        value={searchCountry}
        onChange={handleInputchange}
      />
      <div className="drop-down">
        <select name="region" id="region"  onChange={handleRegionChange}>
          <option value="" disabled selected className="region-name">
            Filter by Region
          </option>
          {regions.map((region) => (
            <option
              key={region}
              value={region.toLowerCase()}
              className="region-name"
            >
              {region}
            </option>
          ))}
        </select>
        {subregions.length > 0 && (
          <select
            name="subregion"
            id="subregion"
            onChange={handleSubregionChange}
          >
            <option value="" disabled selected className="subregion-name">
              Filter by Subregion
            </option>
            {subregions.map((subregion) => (
              <option
                key={subregion}
                value={subregion.toLowerCase()}
                className="subregion-name"
              >
                {subregion}
              </option>
            ))}
          </select>
        )}
      </div>
      <div className="drop-down">
        <select name="populationsort" id="populationsort" onChange={handlePopulationSortChange}>
          <option value="" disabled selected className="region-name">
            Sort by population
          </option>
          <option value="ascending">Asending</option>
          <option value="descending">Descending</option>
        </select>
      </div>
      <div className="drop-down">
        <select name="areasort" id="areasort" onChange={handleAreaSortChange}>
          <option value="" disabled selected className="region-name">
            Sort by Area
          </option>
          <option value="ascending">Asending</option>
          <option value="descending">Descending</option>
        </select>
      </div>
    </div>
  );
}

export default Filter;
