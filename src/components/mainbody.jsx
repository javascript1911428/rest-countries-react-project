import React, { useState, useEffect } from "react";
import "./mainbody.css";
import Filter from "./filter.jsx";
import { useDarkMode } from "./darkmode";
import { Link } from "react-router-dom";

function Mainpage() {
  const [countries, setCountries] = useState([]);
  const [searchCountry, setSearchCountry] = useState("");
  const [filterRegion, setFilterRegion] = useState("");
  const [error, setError] = useState(null);
  const [subregions, setSubregions] = useState([]);
  const [filterSubregion, setFilterSubregion] = useState("");
  const [sortingOrder, setSortingOrder] = useState("descending");
  const [sortingAttribute, setSortingAttribute] = useState("population");

  // console.log(subregions);

  useEffect(() => {
    fetch("https://restcountries.com/v3.1/all")
      .then((data) => {
        if (!data.ok) {
          throw new Error("Network response was not ok");
        }
        return data.json();
      })
      // .then((data) => data.json())
      .then((res) => setCountries(res))
      .catch((error) => {
        setError(error.message);
      });
  }, []);

  const filterSubregions = (selectedRegion) => {
    console.log(selectedRegion);
    const subregions = countries
      .filter((country) => country["region"].toLowerCase() === selectedRegion)
      .map((country) => country["subregion"]);

    const uniqueSubregions = [...new Set(subregions)];
    console.log(uniqueSubregions);
    setSubregions(uniqueSubregions);
  };

  const filteredCountries = countries.filter((each_country) => {
    const countryName = each_country.name.common.toLowerCase();
    const regionMatches =
      filterRegion === "" || each_country.region.toLowerCase() === filterRegion;
    const subregionMatches =
      filterSubregion === "" ||
      (each_country.subregion &&
        each_country.subregion.toLowerCase() === filterSubregion);

    return (
      countryName.includes(searchCountry.toLowerCase()) &&
      regionMatches &&
      subregionMatches
    );
  });

  const regions = [...new Set(countries.map((country) => country.region))];

  const handlePopulationSortChange = (event) => {
    const selectedOrder = event.target.value;
    setSortingAttribute("population");
    setSortingOrder(selectedOrder);
  };

  const handleAreaSortChange = (event) => {
    const selectedOrder = event.target.value;
    setSortingAttribute("area");
    setSortingOrder(selectedOrder);
  };

  const sortCountriesByAttribute = (countries, attribute, order) => {
    return countries.slice().sort((countryA, countryB) => {
      let valueA = 0;
      let valueB = 0;

      if (attribute === "population") {
        valueA = countryA.population;
        valueB = countryB.population;
      } else if (attribute === "area") {
        valueA = countryA.area;
        valueB = countryB.area;
      }

      if (order === "ascending") {
        return valueA - valueB;
      } else {
        return valueB - valueA;
      }
    });
  };

  const sortedCountries = sortCountriesByAttribute(
    filteredCountries,
    sortingAttribute,
    sortingOrder
  );

  const handleInputChange = (event) => {
    setSearchCountry(event.target.value);
  };

  const handleSubregionChange = (event) => {
    const selectedSubregion = event.target.value;
    console.log(selectedSubregion);
    setFilterSubregion(selectedSubregion);
  };

  const handleRegionChange = (event) => {
    const selectedRegion = event.target.value;
    //console.log(selectedRegion);
    setFilterRegion(selectedRegion);
    filterSubregions(selectedRegion);
  };

  const { darkMode } = useDarkMode();

  return (
    <>
      <div className={`main-page ${darkMode ? "light-mode" : "dark-mode"}`}>
        <div>
          <Filter
            handleInputchange={handleInputChange}
            searchCountry={searchCountry}
            handleRegionChange={handleRegionChange}
            regions={regions}
            subregions={subregions}
            handleSubregionChange={handleSubregionChange}
            handlePopulationSortChange={handlePopulationSortChange}
            handleAreaSortChange={handleAreaSortChange}
          />
        </div>
        <div className="country-info-container">
          {error ? (
            <div className="error-message">{error}</div>
          ) : filteredCountries.length === 0 ? (
            <div className="empty-state">No such countries found</div>
          ) : (
            sortedCountries.map((each_country) => {
              const {
                cca2,
                name,
                population,
                region,
                capital,
                flags,
                subregion,
                area,
              } = each_country;
              return (
                <Link key={cca2} to={`/${each_country.name.common}/${each_country.cca2}`}>
                  <div className="each-country" key={cca2}>
                    <img
                      src={flags["png"]}
                      alt="country-flag"
                      className="country-image"
                    />
                    <div className="each-country-info">
                      <h3 className="country-name">{name["common"]}</h3>
                      <h4 className="category">
                        Population:
                        <span className="span-element">{population}</span>
                      </h4>
                      <h4 className="category">
                        Region:<span className="span-element">{region}</span>
                      </h4>
                      <h4 className="category">
                        Capital:<span className="span-element">{capital}</span>
                      </h4>
                      <h4 className="category">
                        Subregion:
                        <span className="span-element">{subregion}</span>
                      </h4>
                      <h4 className="category">
                        Area:<span className="span-element">{area}</span>
                      </h4>
                    </div>
                  </div>
                </Link>
              );
            })
          )}
        </div>
      </div>
    </>
  );
}

export default Mainpage;
