import { useState } from 'react'
import reactLogo from './assets/react.svg'
import viteLogo from '/vite.svg'
import './App.css'
import Homepage from './components/home.jsx'
import Mainpage from './components/mainbody.jsx'
import Loader from './components/loader.jsx'


function App() {
  return(
    <>
      <Loader />
      <Homepage />
      <Mainpage />
    </>
  )
}

export default App
