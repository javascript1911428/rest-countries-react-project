import React from "react";
import ReactDOM from "react-dom/client";
import App from "./App.jsx";
import "./index.css";
import { DarkModeProvider } from "./components/darkmode.jsx";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import CountryDetail from "./components/countrydetail.jsx";

ReactDOM.createRoot(document.getElementById("root")).render(
  <React.StrictMode>
    <BrowserRouter>
      <DarkModeProvider>
        <Routes>
          <Route path="/" element={<App />} />
          <Route path="/:country/:cca2" element={<CountryDetail />} />
        </Routes>
      </DarkModeProvider>
    </BrowserRouter>
  </React.StrictMode>
);
